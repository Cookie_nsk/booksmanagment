﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace AB.Core.Domain
{
    public class GiveOutBook
    {
        public int Id { get; set; }
        [ForeignKey("Book")]
        public int BookId { get; set; }
        public int GiveOutDays { get; set; }
        [ForeignKey("Reader")]
        public int ReaderId { get; set; }
        public DateTime GiveOutDate { get; set; }

        public Book Book { get; set; }
        public Reader Reader { get; set; }

    }
}