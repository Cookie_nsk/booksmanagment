﻿using System;
using System.ComponentModel.DataAnnotations;

namespace AB.Core.Domain
{
    public class Book
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public string Author { get; set; }
        public string Index { get; set; }
        public DateTime Year { get; set; }
    }
}
