﻿using AB.Business;
using AB.Business.Interfaces;
using AB.Core.Domain;
using AutoBookRelease.Models;
using System;
using System.Linq;
using System.Web.Mvc;

namespace AutoBookRelease.Controllers
{
    public class ReaderController : Controller
    {
        private IGenericController<Reader> genericControllerReader;

        public ReaderController(IGenericController<Reader> _iocReaderController)
        {
            genericControllerReader = _iocReaderController ?? throw new Exception("_iocReaderController is null");
        }

        public ActionResult Index()
        {
            return View(genericControllerReader.Get());
        }

        public ActionResult Add(BookModel model)
        {
            return View();
        }
        public ActionResult Update(int readerId)
        {
            Reader reader = genericControllerReader.Get(x => x.Id == readerId).FirstOrDefault();
            return View((ReaderModel)reader);
        }

        public ActionResult AddReader(ReaderModel model)
        {
            ViewBag.addResult = genericControllerReader.Cretate((Reader)model);
            return View("~/Views/Reader/Add.cshtml");
        }
        public ActionResult UpdateReader(ReaderModel model)
        {
            ViewBag.updateResult = genericControllerReader.Update((Reader)model);
            return View("~/Views/Reader/Update.cshtml", model);
        }
        public ActionResult DeleteReader(int readerId)
        {
            genericControllerReader.Delete(readerId);
            return View("~/Views/Reader/Index.cshtml", genericControllerReader.Get());
        }

        public ActionResult SearchReader(string readerName)
        {
            return View("~/Views/Reader/Index.cshtml",
                string.IsNullOrEmpty(readerName) ?
                genericControllerReader.Get() :
                genericControllerReader.Get(x => x.Name.Equals(readerName))
                );
        }
    }
}