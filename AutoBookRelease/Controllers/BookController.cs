﻿using AB.Business;
using AB.Core.Domain;
using AutoBookRelease.Models;
using System.Web.Mvc;
using System.Linq;
using System.Linq.Expressions;
using System.Collections.Generic;
using AB.Business.Interfaces;
using System;

namespace AutoBookRelease.Controllers
{
    public class BookController : Controller
    {
        private IGenericController<Book> genericControlerBook;
        private IGenericController<GiveOutBook> genericControllerGiveOutBook;
        private IGenericController<Reader> genericControllerReader;
        private IGiveOutBooksController giveOutBooksController;

        public BookController(
            IGenericController<Book> _iocBookController,
            IGenericController<Reader> _iocReaderController,
            IGenericController<GiveOutBook> _iocGiveOutBookController,
            IGiveOutBooksController _giveOutBooksController)
        {
            genericControlerBook = _iocBookController ?? throw new Exception("_iocBookController is null");
            genericControllerGiveOutBook = _iocGiveOutBookController ?? throw new Exception("_iocGiveOutBookController is null");
            genericControllerReader = _iocReaderController ?? throw new Exception("_iocReaderController is null");
            giveOutBooksController = _giveOutBooksController ?? throw new Exception("_giveOutBooksController is null");
        }

        public ActionResult Index()
        {
            return View(genericControlerBook.Get());
        }

        public ActionResult Add()
        {
            return View();
        }
        public ActionResult Update(int bookId)
        {
            Book book = genericControlerBook.Get(x => x.Id == bookId).FirstOrDefault();
            return View((BookModel)book);
        }

        public ActionResult AddBook(BookModel model)
        {
            ViewBag.addResult = genericControlerBook.Cretate((Book)model);
            return View("~/Views/Book/Add.cshtml");
        }
        public ActionResult UpdateBook(BookModel model)
        {
            ViewBag.updateResult = genericControlerBook.Update((Book)model);
            return View("~/Views/Book/Update.cshtml", model);
        }
        public ActionResult DeleteBook(int bookId)
        {
            genericControlerBook.Delete(bookId);
            return View("~/Views/Book/Index.cshtml", genericControlerBook.Get());
        }

        public ActionResult SearchBook(string bookName)
        {
            return View("~/Views/Book/Index.cshtml",
                string.IsNullOrEmpty(bookName) ?
                genericControlerBook.Get() :
                genericControlerBook.Get(x => x.Name.Equals(bookName))
                );
        }

        public ActionResult GiveOutBooks()
        {
            SetGiveOutViewBagData();
            return View();
        }
        public ActionResult GiveOutBookAction(GiveOutBookModel model)
        {
            ViewBag.giveOutResult = genericControllerGiveOutBook.Cretate((GiveOutBook)model);
            SetGiveOutViewBagData();
            return View("~/Views/Book/GiveOutBooks.cshtml");
        }
        private void SetGiveOutViewBagData()
        {
            ViewBag.Readers = genericControllerReader.Get()
                .Select(x => new SelectListItem() { Text = x.Name, Value = x.Id.ToString() });
            ViewBag.AvailableBooks = giveOutBooksController.GetAvailableBooks()
                .Select(x => new SelectListItem() { Text = x.Name, Value = x.Id.ToString() });
            ViewBag.GivenOutBooks = giveOutBooksController.Get();
        }

    }
}