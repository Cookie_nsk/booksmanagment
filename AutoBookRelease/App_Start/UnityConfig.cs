﻿using AB.Business;
using AB.Business.Interfaces;
using System.Web.Mvc;
using Unity;
using Unity.Mvc5;

namespace AutoBookRelease.App_Start
{
    public static class UnityConfig
    {
        public static void RegisterComponents()
        {
            var container = new UnityContainer();

            container.RegisterType(typeof(IGenericController<>), typeof(GenericController<>));
            container.RegisterType<IGiveOutBooksController, GiveOutBooksController>();

            DependencyResolver.SetResolver(new UnityDependencyResolver(container));
        }
    }
}