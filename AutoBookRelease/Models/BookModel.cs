﻿using AB.Core.Domain;
using System;
using System.ComponentModel.DataAnnotations;

namespace AutoBookRelease.Models
{
    public class BookModel
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Заполните поле \"Наименование\"")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Заполните поле \"Автор\"")]
        public string Author { get; set; }

        [Required(ErrorMessage = "Заполните поле \"Артикул\"")]
        public string Index { get; set; }

        [Required(ErrorMessage = "Заполните поле \"Год\"")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd.MM.yyyy}")]
        public DateTime Year { get; set; }

        public static explicit operator Book(BookModel model)
        {
            return new Book()
            {
                Id = model.Id,
                Author = model.Author,
                Index = model.Index,
                Name = model.Name,
                Year = model.Year,
            };
        }

        public static explicit operator BookModel(Book model)
        {
            return new BookModel()
            {
                Id = model.Id,
                Author = model.Author,
                Index = model.Index,
                Name = model.Name,
                Year = model.Year,
            };
        }

    }
}