﻿using AB.Core.Domain;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace AutoBookRelease.Models
{
    public class GiveOutBookModel
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Заполните поле \"Книга\"")]
        public int BookId { get; set; }

        [Required(ErrorMessage = "Заполните поле \"Количество дней\"")]
        public int GiveOutDays { get; set; }

        [Required(ErrorMessage = "Заполните поле \"Читатель\"")]
        public int ReaderId { get; set; }

        [Required(ErrorMessage = "Заполните поле \"Дата выдачи\"")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd.MM.yyyy}")]
        public DateTime GiveOutDate { get; set; }

        public static explicit operator GiveOutBook(GiveOutBookModel model)
        {
            return new GiveOutBook()
            {
                Id = model.Id,
                BookId = model.BookId,
                GiveOutDate = model.GiveOutDate,
                GiveOutDays = model.GiveOutDays,
                ReaderId = model.ReaderId,
            };
        }

        public static explicit operator GiveOutBookModel(GiveOutBook model)
        {
            return new GiveOutBookModel()
            {
                Id = model.Id,
                BookId = model.BookId,
                GiveOutDate = model.GiveOutDate,
                GiveOutDays = model.GiveOutDays,
                ReaderId = model.ReaderId,
            };
        }

    }
}