﻿using AB.Core.Domain;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AutoBookRelease.Models
{
    public class ReaderModel
    {
        public int Id { get; set; }
        [Required(ErrorMessage ="Заполните поле \"Имя\"")]
        public string ReaderName { get; set; }
        [Required(ErrorMessage = "Заполните поле \"Дата рождения\"")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd.MM.yyyy}")]
        public DateTime BirthDate { get; set; }

        public static explicit operator Reader(ReaderModel model)
        {
            return new Reader()
            {
                Id = model.Id,
                BirthDate = model.BirthDate,
                Name = model.ReaderName,
            };
        }

        public static explicit operator ReaderModel(Reader model)
        {
            return new ReaderModel()
            {
                Id = model.Id,
                BirthDate = model.BirthDate,
                ReaderName = model.Name,
            };
        }
    }
}