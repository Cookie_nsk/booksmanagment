﻿using AB.Core.Domain;
using System.Data.Entity;

namespace AB.DataBaseContext
{
    public class DefaultContext : System.Data.Entity.DbContext
    {
        public DbSet<Book> Books { get; set; }
        public DbSet<Reader> Reader { get; set; }
        public DbSet<GiveOutBook> GiveOutBook { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //modelBuilder.Entity<GiveOutBook>()
            //    .HasRequired(b => b.Book)
            //    .;

            //modelBuilder.Entity<GiveOutBook>()
            //    .HasRequired(b => b.Reader);
        }
    }
}