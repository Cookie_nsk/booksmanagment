﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AB.Business.Interfaces
{
    public interface IGenericController<T> where T : class
    {
        IEnumerable<T> Get();

        IEnumerable<T> Get(Func<T, bool> predicate);

        T FindById(int id);

        bool Cretate(T item);

        bool Update(T item);

        bool Delete(int id);

    }
}
