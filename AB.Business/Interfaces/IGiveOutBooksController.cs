﻿using AB.Core.Domain;
using System.Collections.Generic;

namespace AB.Business.Interfaces
{
    public interface IGiveOutBooksController
    {
        IEnumerable<Book> GetAvailableBooks();
        IEnumerable<GiveOutBook> Get();
    }
}
