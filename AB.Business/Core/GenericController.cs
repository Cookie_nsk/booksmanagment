﻿using AB.Business.Interfaces;
using AB.DataBaseContext;
using AB.DbContext.Interfaces;
using NlogHelper;
using System;
using System.Collections.Generic;

namespace AB.Business
{
    public class GenericController<T> : IGenericController<T> where T : class
    {
        public IEnumerable<T> Get()
        {
            try
            {
                using (DefaultContext db = new DefaultContext())
                {
                    IGenericRepository<T> repository = new GenericRepository<T>(db);
                    return repository.Get();
                }
            }
            catch (Exception exc)
            {
                NLogHelper.Error(exc);
                return null;
            }
        }
        public IEnumerable<T> Get(Func<T, bool> predicate)
        {
            try
            {
                using (DefaultContext db = new DefaultContext())
                {
                    IGenericRepository<T> repository = new GenericRepository<T>(db);
                    return repository.Get(predicate);
                }
            }
            catch (Exception exc)
            {
                NLogHelper.Error(exc);
                return null;
            }
        }
        public T FindById(int id)
        {
            try
            {
                using (DefaultContext db = new DefaultContext())
                {
                    IGenericRepository<T> repository = new GenericRepository<T>(db);
                    return repository.FindById(id);
                }
            }
            catch (Exception exc)
            {
                NLogHelper.Error(exc);
                return null;
            }
        }
        public bool Cretate(T item)
        {
            try
            {
                using (DefaultContext db = new DefaultContext())
                {
                    IGenericRepository<T> repository = new GenericRepository<T>(db);
                    repository.Create(item);
                }
            }
            catch (Exception exc)
            {
                NLogHelper.Error(exc);
                return false;
            }

            return true;
        }
        public bool Update(T item)
        {
            try
            {
                using (DefaultContext db = new DefaultContext())
                {
                    IGenericRepository<T> repository = new GenericRepository<T>(db);
                    repository.Update(item);
                }
            }
            catch (Exception exc)
            {
                NLogHelper.Error(exc);
                return false;
            }

            return true;
        }
        public bool Delete(int id)
        {
            try
            {
                using (DefaultContext db = new DefaultContext())
                {
                    IGenericRepository<T> repository = new GenericRepository<T>(db);
                    T itemToDelete = repository.FindById(id);
                    repository.Delete(itemToDelete);
                }
            }
            catch (Exception exc)
            {
                NLogHelper.Error(exc);
                return false;
            }

            return true;
        }

    }
}
