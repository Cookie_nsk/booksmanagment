﻿using AB.Business.Interfaces;
using AB.Core.Domain;
using AB.DataBaseContext;
using AB.DbContext.Interfaces;
using NlogHelper;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace AB.Business
{
    public class GiveOutBooksController : IGiveOutBooksController
    {
        public IEnumerable<Book> GetAvailableBooks()
        {
            try
            {
                using (DefaultContext db = new DefaultContext())
                {
                    IGenericRepository<GiveOutBook> repository = new GenericRepository<GiveOutBook>(db);
                    IGenericRepository<Book> bookRepo = new GenericRepository<Book>(db);

                    IQueryable<int> goBooks = repository.Get().Select(x => x.BookId).AsQueryable();

                    return bookRepo.Get(x => !goBooks.Contains(x.Id));
                }
            }
            catch (Exception exc)
            {
                NLogHelper.Error(exc);
                return null;
            }
        }

        public IEnumerable<GiveOutBook> Get()
        {
            try
            {
                using (DefaultContext db = new DefaultContext())
                {
                    return db.GiveOutBook
                        .Include("Book")
                        .Include("Reader")
                        .ToList(); 
                }
            }
            catch (Exception exc)
            {
                NLogHelper.Error(exc);
                return null;
            }
        }

    }
}
