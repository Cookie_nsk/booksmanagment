﻿using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NlogHelper
{
    public static class NLogHelper
    {
        public static void Error(Exception exc)
        {
            LogManager.GetCurrentClassLogger().Error(exc);
        }

        public static void Warning(string message)
        {
            LogManager.GetCurrentClassLogger().Warn(message);
        }
    }
}
